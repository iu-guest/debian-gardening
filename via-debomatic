#!/usr/bin/python3
## Copyright 2018 Dmitry Bogatov <KAction@gnu.org>
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
import subprocess
import sys, os
import os.path
import debian
import debian.changelog
import hashlib
import dbm.gnu as gdbm

class force:
    @staticmethod
    def unlink(path):
        "Remove the file at {path}. Do nothing, if there is none."
        try:
            os.unlink(path)
        except FileNotFoundError:
            pass

class DebInfo:
    def __init__(self):
        with open('debian/changelog') as f:
            changelog = debian.changelog.Changelog(f)
            latest = changelog[0]
            self.version = latest.version
            self.package = latest.package
            self.distribution = latest.distributions
            self.dsc = "../{package}_{version}.dsc".format_map(self.__dict__)

class GitInfo:
    def __init__(self):
        # Yes, typeof(self.head) is bytes, not str. It shortens code
        # that deals with gdbm.
        self.head = subprocess.check_output(['git', 'log', '-1',
            '--pretty=%H']).decode('utf-8').strip().encode('utf-8')

class Debomatic:
    def __init__(self, debinfo, arch):
        base = 'http://debomatic-{arch}.debian.net/debomatic/{dist}/pool/{package}_{version}'
        self.content = base + '/{package}_{version}.content'
        def fmt(string):
            return string.format(arch=arch, 
                                 dist=debinfo.distribuion, 
                                 package=debinfo.package, 
                                 version=debinfo.version)

        self.content = fmt(self.content)

def fatal(err, *args, **kwargs):
    "Write formatted error message on stderr and exit program unsuccesfully."
    sys.stderr.write('via-debomatic: ')
    sys.stderr.write(err.format(*args, **kwargs))
    sys.stderr.write('\n')
    sys.exit(1)

def ensure_exists(path):
    "Check that {path} exists, and trigger fatal error otherwise."
    err = 'path "{0}" not found. Are you sure you are in the correct directory?'
    if not os.path.exists(path):
        fatal(err, path)

def ensure_clean_tree():
    "Check that git working tree is clean, and trigger fatal error otherwise."
    status = subprocess.check_output(['git', 'status',
        '--porcelain=2']).decode('utf-8').strip()
    if status != "":
        fatal('working tree is dirty. Refuse to continue.\n' + status)

def file_sha1(path):
    "Return sha1 digest (binary string) of file content."
    with open(path, 'rb') as f:
        return hashlib.sha1(f.read()).digest()

def create_source_package(debinfo, gitinfo):
    """Create signed source package from git HEAD.

    This function uses cache to not re-create source package,
    if it is up-to-date, It should only improve perfomance.
    """
    cache_path = os.path.expanduser('~/.cache/via-debomatic.dbm')
    need_build = True
    if os.path.exists(cache_path) and os.path.exists(debinfo.dsc):
        with gdbm.open(cache_path) as db:
            if db.get(gitinfo.head) == file_sha1(debinfo.dsc):
                need_build = False
    if need_build:
        subprocess.check_call(['dgit', 'build', '-S', '-sa'])
        subprocess.check_call(['debsign', '-S'])
        with gdbm.open(cache_path, 'c') as db:
            db[gitinfo.head] = file_sha1(debinfo.dsc)

ensure_exists('debian/control')
ensure_exists('debian/changelog')
ensure_exists('.git')

# Building of source package, via `dgit build -S` creates 'debian/files',
# rendering working tree dirty. Many tools, dgit(1) in particular, dislike it.
#
# This is little nicety to not annoy developer, who just built source package
# without us (without `via-debomatic').
force.unlink('debian/files')

ensure_clean_tree()
debinfo = DebInfo()
gitinfo = GitInfo()

create_source_package(debinfo, gitinfo)

# Because I use GPG key for ssh authentification, and I need this.
subprocess.check_output(['gpg-connect-agent', 'updatestartuptty', '/bye'])

# FIXME: Here we assume, that ~/.dupload.conf contains configuration for
# debomatic-i386, debomatic-amd64 and so forth. It would be nice to
# be more stand-alone.

architectures = ['i386']
try:
    if sys.argv[1] == 'every':
        architectures = 'amd64 i386 arm64 armel armhf mips64el mips mipsel powerpc ppc64el s390x'.split(' ')
except IndexError:
    pass

for arch in architectures:
    print("\033[31mARCH: %s\033[0m" % arch)
    subprocess.check_call(['debrelease', '-S', '--dput', '--force', 'debomatic-%s' % arch])

for arch in architectures:
    print("\033[31mLoading in browser: %s\033[0m" % arch)
    url = 'http://debomatic-{arch}.debian.net/distribution#{distribution}/{package}/{version}'
    url = url.format(arch=arch, **debinfo.__dict__)
    subprocess.check_output(['chromium', url])

# vim: tw=78
